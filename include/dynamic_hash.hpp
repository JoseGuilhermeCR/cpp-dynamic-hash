/* MIT License
 *
 * Copyright (c) 2021 José Guilherme de C. Rodrigues
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

/**
 * DynamicHash API:
 * DynamicHash(uint32_t init_global_depth,
 *             uint32_t bucket_element_number,
 *             const std::string& main_name,
 *             const std::string& index_name,
 *             const std::string& dir_name);
 * bool insert(const T& t);
 * bool remove(uint32_t id);
 * bool read(uint32_t id, T& t);
 * bool contains(const T& t, bool lazy = false);
 */

#pragma once

#include <cmath>
#include <cstdint>
#include <exception>
#include <fstream>
#include <ios>
#include <iostream>
#include <memory>
#include <optional>
#include <stdexcept>
#include <type_traits>
#include <vector>

template <typename T, typename Traits>
concept Storable = requires(T t, T v, std::fstream file)
{
    {
        Traits::hash_value(t)
        } -> std::same_as<uint32_t>;
    {
        Traits::write_bytes(t, file)
        } -> std::same_as<void>;
    {
        Traits::read_bytes(t, file)
        } -> std::same_as<void>;
    {
        t == v
        } -> std::same_as<bool>;
};

struct Bucket {
    Bucket(uint32_t number_of_elements) : address(), local_depth()
    {
        constexpr auto invalid = std::numeric_limits<uint32_t>::max();
        for (uint32_t u = 0; u != number_of_elements; ++u) {
            ids.push_back(invalid);
            entries.push_back(invalid);
        }
    }

    Bucket(Bucket&&)      = delete;
    Bucket(const Bucket&) = delete;

    uint32_t address;
    uint32_t local_depth;

    std::vector<uint32_t> ids;
    std::vector<uint32_t> entries;
};

class Directory
{
    struct DirEntry {
        uint32_t bucket_addr;
    };

   public:
    Directory(std::fstream& dir_file) : m_file(dir_file)
    {
        dir_file.seekg(0, std::ios_base::beg);
        dir_file.read(reinterpret_cast<char*>(&m_global_depth),
                      sizeof(m_global_depth));

        while (!dir_file.eof()) {
            uint32_t b_addr;
            dir_file.read(reinterpret_cast<char*>(&b_addr), sizeof(b_addr));
            m_entries.emplace_back(b_addr);
        }
        // An extra entry is read. Remove it.
        m_entries.pop_back();
    }

    Directory(std::fstream& dir_file,
              uint32_t global_depth,
              uint32_t bucket_size)
        : m_file(dir_file),
          m_global_depth(global_depth)
    {
        const uint32_t entry_number =
            static_cast<uint32_t>(pow(2.0, static_cast<double>(global_depth)));
        for (uint32_t u = 0; u != entry_number; ++u) {
            m_entries.emplace_back(u * bucket_size);
        }

        save_to_file();
    }

    void duplicate()
    {
        const uint32_t original_size = static_cast<uint32_t>(m_entries.size());

        for (uint32_t u = 0; u != original_size; ++u) {
            DirEntry entry = m_entries.at(u);
            m_entries.push_back(entry);
        }

        ++m_global_depth;
    }

    void update_pointers(uint32_t original_addr, uint32_t new_addr)
    {
        for (uint32_t u = m_entries.size() / 2; u != m_entries.size(); ++u) {
            if (m_entries.at(u).bucket_addr == original_addr) {
                m_entries.at(u).bucket_addr = new_addr;
            }
        }
    }

    void save_to_file()
    {
        m_file.seekg(0, std::ios_base::beg);
        m_file.write(reinterpret_cast<const char*>(&m_global_depth),
                     sizeof(m_global_depth));

        for (const auto& entry : m_entries) {
            m_file.write(reinterpret_cast<const char*>(&entry.bucket_addr),
                         sizeof(entry.bucket_addr));
        }
    }

    uint32_t number_of_entries()
    {
        return m_entries.size();
    }

    std::optional<uint32_t> get_bucket_address(uint32_t number)
    {
        if (number >= m_entries.size()) {
            return std::nullopt;
        }

        return m_entries.at(number).bucket_addr;
    }

    uint32_t global_depth()
    {
        return m_global_depth;
    }

   private:
    void print()
    {
        std::cout << "===Directory===" << '\n';
        std::cout << "Global Depth: " << m_global_depth << '\n';
        uint32_t u = 0;
        for (const auto& entry : m_entries) {
            std::cout << "Bucket: " << u++
                      << " BucketAddress: " << entry.bucket_addr << '\n';
        }
    }
    std::fstream& m_file;

    uint32_t m_global_depth;
    std::vector<DirEntry> m_entries;
};

template <typename T, typename TraitsForT>
requires Storable<T, TraitsForT>
class DynamicHash
{
   public:
    DynamicHash(uint32_t init_global_depth,
                uint32_t bucket_element_number,
                const std::string& main_name,
                const std::string& index_name,
                const std::string& dir_name)
        : m_bucket_element_number(bucket_element_number),
          m_directory(nullptr)
    {
        if (bucket_element_number == 0) {
            throw std::invalid_argument(
                "Bucket element number must be non zero.");
        }

        m_main_file.open(
            main_name,
            std::ios_base::binary | std::ios_base::in | std::ios_base::out);
        m_index_file.open(
            index_name,
            std::ios_base::binary | std::ios_base::in | std::ios_base::out);
        m_dir_file.open(
            dir_name,
            std::ios_base::binary | std::ios_base::in | std::ios_base::out);

        if (!m_main_file.is_open() || !m_index_file.is_open()
            || !m_dir_file.is_open()) {
            m_main_file.open(main_name,
                             std::ios_base::binary | std::ios_base::in
                                 | std::ios_base::out | std::ios_base::trunc);
            m_index_file.open(index_name,
                              std::ios_base::binary | std::ios_base::in
                                  | std::ios_base::out | std::ios_base::trunc);
            m_dir_file.open(dir_name,
                            std::ios_base::binary | std::ios_base::in
                                | std::ios_base::out | std::ios_base::trunc);

            if (!m_main_file.is_open() || !m_index_file.is_open()
                || !m_dir_file.is_open()) {
                throw std::runtime_error("Could not create necessary files.");
            }
        }

        const auto file_size = [](std::fstream& file) {
            file.seekp(0, std::ios_base::end);
            const auto pos = file.tellp();
            file.seekp(0, std::ios_base::beg);

            return static_cast<size_t>(pos);
        };

        const auto main_size  = file_size(m_main_file);
        const auto index_size = file_size(m_index_file);
        const auto dir_size   = file_size(m_dir_file);

        if (main_size == 0 || index_size == 0 || dir_size == 0) {
            // Local Depth + Size for each entry (32bit value that will be
            // hashed + entry in main file).
            const uint32_t bucket_size =
                sizeof(uint32_t)
                + bucket_element_number * (sizeof(uint32_t) + sizeof(uint32_t));
            m_directory = std::make_unique<Directory>(m_dir_file,
                                                      init_global_depth,
                                                      bucket_size);

            // Create initial index file.
            const uint32_t local_bucket_depth = init_global_depth;
            constexpr uint32_t invalid = std::numeric_limits<uint32_t>::max();
            for (uint32_t u = 0; u != m_directory->number_of_entries(); ++u) {
                // Local depth of bucket.
                m_index_file.write(
                    reinterpret_cast<const char*>(&local_bucket_depth),
                    sizeof(local_bucket_depth));
                for (uint32_t v = 0; v != bucket_element_number; ++v) {
                    // Write value that will be hashed.
                    m_index_file.write(reinterpret_cast<const char*>(&invalid),
                                       sizeof(invalid));
                    // Write entry number in main file.
                    m_index_file.write(reinterpret_cast<const char*>(&invalid),
                                       sizeof(invalid));
                }
            }

            const uint32_t entries = 0;
            m_main_file.write(reinterpret_cast<const char*>(&entries),
                              sizeof(entries));
        } else {
            m_directory = std::make_unique<Directory>(m_dir_file);
        }
    }

    /**
     * @brief Checks if an entry is contained by the hash.
     *
     * @param t Entry that will be checked.
     * @param lazy Whether only a hash value check is enough (false), or is it
     * needed to read the whole object and use the == operator to check if both
     * entries are equal (true).
     * @return true It's contained.
     * @return false It's not contained.
     */
    bool contains(const T& t, bool lazy = false)
    {
        const uint32_t hash_value = TraitsForT::hash_value(t);
        const uint32_t entry_number =
            hash_value % m_directory->number_of_entries();

        Bucket bucket(m_bucket_element_number);
        if (!load_bucket(bucket, entry_number)) {
            return false;
        }

        for (uint32_t u = 0; u != m_bucket_element_number; ++u) {
            if (bucket.ids.at(u) == hash_value) {
                if (!lazy) {
                    const uint32_t address = bucket.entries.at(u);
                    m_main_file.seekp(address, std::ios_base::beg);

                    // A little trick so we don't default construct it.
                    uint8_t storage[sizeof(T)];
                    T* read_t = reinterpret_cast<T*>(storage);
                    // Read the T entry on main file.
                    TraitsForT::read_bytes(*read_t, m_main_file);
                    if (*read_t == t) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    // Hash is more than enough for lazy contains.
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @brief Insert an entry in the hash.
     *
     * @param t Entry that will be inserted.
     * @return true Successfully inserted.
     * @return false Couldn't insert because an entry with the same hash_value
     * was already in the hash or some other error happened.
     */
    bool insert(const T& t)
    {
        if (contains(t)) {
            return false;
        }

        // Read number of entries in main file.
        uint32_t entries = 0;
        m_main_file.seekp(0, std::ios_base::beg);
        m_main_file.read(reinterpret_cast<char*>(&entries), sizeof(entries));

        // Increment it.
        const uint32_t updated_entries = entries + 1;
        m_main_file.seekg(0, std::ios_base::beg);
        m_main_file.write(reinterpret_cast<const char*>(&updated_entries),
                          sizeof(updated_entries));

        // Go to the end and let t be written to the file.
        m_main_file.seekg(0, std::ios_base::end);
        const uint32_t write_address =
            static_cast<uint32_t>(m_main_file.tellg());
        TraitsForT::write_bytes(t, m_main_file);

        const uint32_t hash_value = TraitsForT::hash_value(t);
        const uint32_t entry_number =
            hash_value % m_directory->number_of_entries();
        return recursive_insert(hash_value, entry_number, write_address);
    }

    /**
     * @brief Reads an entry from the hash.
     *
     * @param id ID of entry to be read.
     * @param t Where the read value will be stored.
     * @return true Sucessfully read and the value in t is valid.
     * @return false Error when reading. The value in t is unknown.
     */
    bool read(uint32_t id, T& t)
    {
        const uint32_t entry_number = id % m_directory->number_of_entries();

        Bucket bucket(m_bucket_element_number);
        if (!load_bucket(bucket, entry_number)) {
            return false;
        }

        for (uint32_t u = 0; u != m_bucket_element_number; ++u) {
            if (bucket.ids.at(u) == id) {
                const uint32_t address = bucket.entries.at(u);
                m_main_file.seekp(address, std::ios_base::beg);
                TraitsForT::read_bytes(t, m_main_file);
                return true;
            }
        }

        return false;
    }

    /**
     * @brief Removes an entry from the hash. Be aware that the entry is not
     * removed from the main file.
     *
     * @param id Id of the entry that will be removed.
     * @return true Successfully removed.
     * @return false Couldn't remove. The entry never existed in first place or
     * a misterious error has happened.
     */
    bool remove(uint32_t id)
    {
        const uint32_t entry_number = id % m_directory->number_of_entries();

        Bucket bucket(m_bucket_element_number);
        if (!load_bucket(bucket, entry_number)) {
            return false;
        }

        constexpr uint32_t invalid = std::numeric_limits<uint32_t>::max();

        for (uint32_t u = 0; u != m_bucket_element_number; ++u) {
            if (bucket.ids.at(u) == id) {
                // Invalidate this bucket entry.
                bucket.ids.at(u)     = invalid;
                bucket.entries.at(u) = invalid;

                // Save it to disk.
                save_bucket(bucket);

                return true;
            }
        }

        return false;
    }

    friend std::ostream& operator<<(std::ostream& os,
                                    DynamicHash<T, TraitsForT>& hash)
    {
        hash.print(os);
        return os;
    }

   private:
    bool recursive_insert(uint32_t hash_value,
                          uint32_t entry_number,
                          uint32_t write_address)
    {
        Bucket bucket(m_bucket_element_number);
        if (!load_bucket(bucket, entry_number)) {
            return false;
        }

        bool free_pos_found = false;
        for (uint32_t u = 0; u != m_bucket_element_number; ++u) {
            constexpr uint32_t invalid = std::numeric_limits<uint32_t>::max();
            if (bucket.ids.at(u) == invalid
                && bucket.entries.at(u) == invalid) {
                bucket.ids.at(u)     = hash_value;
                bucket.entries.at(u) = write_address;
                free_pos_found       = true;
                break;
            }
        }

        bool success = true;

        if (free_pos_found) {
            save_bucket(bucket);
        } else {
            if (m_directory->global_depth() == bucket.local_depth) {
                m_directory->duplicate();
                m_directory->save_to_file();
            }

            ++bucket.local_depth;

            // Create new bucket.
            Bucket new_bucket(m_bucket_element_number);
            new_bucket.local_depth = bucket.local_depth;
            save_bucket(new_bucket, true);

            // The directory has been duplicated and all entries in it's second
            // half are pointing to the same bucket as their corresponding
            // entries in the first half. A new bucket has been added, and now
            // half the entries from the original bucket must point to it.
            m_directory->update_pointers(bucket.address, new_bucket.address);

            // Now we need to rehash the entries from the original bucket and
            // see if they go to the newly created bucket or stay at the
            // original.

            // First make a copy of the original bucket's content.
            std::vector<uint32_t> ids     = bucket.ids;
            std::vector<uint32_t> entries = bucket.entries;

            ids.push_back(hash_value);
            entries.push_back(write_address);

            // Clean original bucket.
            constexpr uint32_t invalid = std::numeric_limits<uint32_t>::max();
            for (auto& id : bucket.ids) {
                id = invalid;
            }

            for (auto& entry : bucket.entries) {
                entry = invalid;
            }

            // Rehash.
            uint32_t bucket_count     = 0;
            uint32_t new_bucket_count = 0;

            for (uint32_t u = 0; u != ids.size(); ++u) {
                if (bucket_count >= bucket.ids.size()
                    || new_bucket_count >= new_bucket.ids.size()) {
                    save_bucket(bucket);
                    save_bucket(new_bucket);

                    uint32_t original_number_of_entries =
                        m_directory->number_of_entries();
                    success =
                        recursive_insert(ids.at(u),
                                         ids.at(u) % original_number_of_entries,
                                         entries.at(u));

                    // FIXME: Is it necessary? :thinking:
                    // Forcefully load the bucket after the recursion because it
                    // might've been altered.
                    load_bucket(bucket,
                                bucket.ids.at(0) % original_number_of_entries);
                    load_bucket(
                        new_bucket,
                        new_bucket.ids.at(0) % original_number_of_entries);
                    break;
                }

                if (ids.at(u) % m_directory->number_of_entries()
                    != entry_number) {
                    // If the id is not equal to the original entry number,
                    // it's now in the new bucket.
                    new_bucket.ids.at(new_bucket_count)     = ids.at(u);
                    new_bucket.entries.at(new_bucket_count) = entries.at(u);
                    ++new_bucket_count;
                } else {
                    bucket.ids.at(bucket_count)     = ids.at(u);
                    bucket.entries.at(bucket_count) = entries.at(u);
                    ++bucket_count;
                }
            }

            save_bucket(bucket);
            save_bucket(new_bucket);
        }

        return success;
    }

    bool load_bucket(Bucket& bucket, uint32_t entry_number)
    {
        auto addr_or_nothing = m_directory->get_bucket_address(entry_number);
        if (!addr_or_nothing.has_value()) {
            return false;
        }

        bucket.address = addr_or_nothing.value();
        m_index_file.seekg(bucket.address, std::ios_base::beg);

        m_index_file.read(reinterpret_cast<char*>(&bucket.local_depth),
                          sizeof(bucket.local_depth));
        for (uint32_t u = 0; u != m_bucket_element_number; ++u) {
            uint32_t id;
            uint32_t entry;

            m_index_file.read(reinterpret_cast<char*>(&bucket.ids.at(u)),
                              sizeof(id));
            m_index_file.read(reinterpret_cast<char*>(&bucket.entries.at(u)),
                              sizeof(entry));
        }

        return true;
    }

    void save_bucket(Bucket& bucket, bool on_end = false)
    {
        if (!on_end) {
            m_index_file.seekp(bucket.address, std::ios_base::beg);
        } else {
            m_index_file.seekp(0, std::ios_base::end);
            bucket.address = static_cast<uint32_t>(m_index_file.tellp());
        }

        m_index_file.write(reinterpret_cast<const char*>(&bucket.local_depth),
                           sizeof(bucket.local_depth));
        for (uint32_t u = 0; u != m_bucket_element_number; ++u) {
            m_index_file.write(reinterpret_cast<const char*>(&bucket.ids.at(u)),
                               sizeof(bucket.ids.at(0)));
            m_index_file.write(
                reinterpret_cast<const char*>(&bucket.entries.at(u)),
                sizeof(bucket.entries.at(0)));
        }
    }

    void print(std::ostream& stream)
    {
        for (uint32_t u = 0; u != m_directory->number_of_entries(); ++u) {
            Bucket bucket(m_bucket_element_number);
            load_bucket(bucket, u);
            stream << "\tBucket number " << u << " at address "
                   << bucket.address << '\n';
            for (uint32_t v = 0; v != m_bucket_element_number; ++v) {
                stream << "Entry: " << v << " | ID: " << bucket.ids.at(v)
                       << " | ADDR: " << bucket.entries.at(v) << '\n';
            }
            stream << '\n';
        }
    }

    uint16_t m_bucket_element_number;

    std::unique_ptr<Directory> m_directory;

    std::fstream m_main_file;
    std::fstream m_index_file;
    std::fstream m_dir_file;
};
