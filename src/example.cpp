#include "dynamic_hash.hpp"

#include <iostream>

struct Person {
    uint32_t id;
    uint8_t age;

    friend bool operator==(const Person& lhs, const Person& rhs)
    {
        return lhs.id == rhs.id;
    }
};

struct PersonTraits {
    /* Should return the unique value that will be hashed. */
    static uint32_t hash_value(const Person& person)
    {
        return person.id;
    }

    static void write_bytes(const Person& person, std::fstream& file)
    {
        file.write(reinterpret_cast<const char*>(&person.id),
                   sizeof(person.id));
        file.write(reinterpret_cast<const char*>(&person.age),
                   sizeof(person.age));
    }

    static void read_bytes(Person& person, std::fstream& file)
    {
        file.read(reinterpret_cast<char*>(&person.id), sizeof(person.id));
        file.read(reinterpret_cast<char*>(&person.age), sizeof(person.age));
    }
};

int main([[maybe_unused]] int argc, [[maybe_unused]] const char* const argv[])
{
    DynamicHash<Person, PersonTraits> hash(1,
                                           30,
                                           "main.bin",
                                           "index.bin",
                                           "dir.bin");

    Person me{.id = 0, .age = 21};
    Person you{.id = 1, .age = 18};

    hash.insert(me);
    hash.insert(you);

    /* Compare the hash value. */
    [[maybe_unused]] const bool contains = hash.contains(me);
    /* Calls the == operator on the object, it will need to load
     * the object from the main file (using PersonTraits::read_bytes). */
    [[maybe_unused]] const bool lazy_contains = hash.contains(me, true);

    Person same_as_me;
    hash.read(me.id, same_as_me);
    hash.remove(me.id);

    return 0;
}
