# Lista de arquivos que serão compilados.
SRC_DIRS := src
SRC := $(foreach DIR,$(SRC_DIRS),$(wildcard $(DIR)/*.cpp))

# Arquivos .o e .d correspondentes.
BUILD_DIR := build/
OBJ := $(subst .cpp,.o,$(addprefix $(BUILD_DIR), $(SRC)))
DEP := $(subst .cpp,.d,$(addprefix $(BUILD_DIR), $(SRC)))

# Compilador, linker e assembler.
CXX := $(PREFIX)g++
LD := $(PREFIX)ld
AS := $(PREFIX)as

# Agora defina o prefixo padrão.
PREFIX ?= x86_64-linux-

# Diretório do output.
OUTPUT_DIR := $(PREFIX:-=)

# Saída da compilação.
OUTPUT := $(OUTPUT_DIR)/dynamic_hash

# Flags do compilador.
CXX_FLAGS += -Wall -Wextra --std=c++20 -MMD -g3
# Flags do pré-processador.
PP_FLAGS += -Iinclude/ -DDEBUG
# Flags do linker.
LD_FLAGS += -lm

$(BUILD_DIR)%.o: %.cpp
# Fixme: Não usar mkdir para cada arquivo...
	mkdir -p $(@D) && $(CXX) $(CXX_FLAGS) $(PP_FLAGS) -c $< -o $@

$(OUTPUT): $(OBJ)
	mkdir -p $(OUTPUT_DIR)
	$(CXX) $^ $(LD_FLAGS) -o $@

-include $(DEP)

.PHONY: cleanbuild
cleanbuild:
	rm -rf $(BUILD_DIR)

.PHONY: clean
clean:
	rm -f *.bin
	rm -rf $(OUTPUT_DIR) $(BUILD_DIR)

.PHONY: run
run: $(OUTPUT)
	./$(OUTPUT)

.PHONY: debug
debug: $(OUTPUT)
	gdb ./$(OUTPUT)

